module bot

go 1.18

require github.com/go-python/gpython v0.0.3-0.20191118123038-4c28217f7422

require (
	github.com/olekukonko/tablewriter v0.0.5
	github.com/pelletier/go-toml v1.9.4
)

require github.com/mattn/go-runewidth v0.0.9 // indirect
